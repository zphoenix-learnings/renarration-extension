/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.html',
    './src/**/*.jsx',
    './src/**/*.tsx',
    // Add more file patterns if necessary
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  purge: [],
  darkMode: false, // or 'media' or 'class'
  variants: {},
};
