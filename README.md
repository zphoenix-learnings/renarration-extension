# Renarration Browser Extension

Renarration Browser Extension is a browser addon and follow the instructions.

## Setup & Installation

1. Clone this repository.
2. Run the app
    ```
    $ yarn run build
    Note: After successfully running this command it would have created `public` folder with neccessary files.
    ```
    ![](https://pad.ccc-p.org/uploads/74a4e34e-d0e6-492a-a6d3-a7207016e3c7.png)
       Output after successful build

3. Go to a Browser's Extension Manager and load the build folder.
    **a. In Chromium :**
    
    To test our extension we have to open the Extension Page. We do that by typing `chrome://extensions/` in the Chromium URL bar. Next, we switch `Developer Mode` on that enables us to test our extension. Click on `Load unpack` which would open a directory picker. Navigate to the build folder of the extension, select the `build` directory and press `Open`.
    The extensions would be loaded and you can find in the extensions list in your browser
    
    **b. In Firefox :**
    To test our extension we have to open the Firefox Debugging tool. We do   that by typing `about:debugging` in the Firefox URL bar. Next, we click on     the `This Firefox` tab which provides information about the extensions and     running workers. Our focus will be on the top of the page, more             specifically on the `Temporary Extensions panel`.

    Click on `Load Temporary Add-on…` which should open a file picker. Navigate to the build folder of the extension, select the `index.html` and press `Open`.
    