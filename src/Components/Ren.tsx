import axios from "axios";
import React, { useState } from "react";

const Ren = ({ data }: { data: any }) => {
  const [url, setUrl] = useState(data);
  const [article, setArticle] = useState<any>(null);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const options = {
      method: "POST",
      url: "http://pantoto.net:5000/api/parse",
      headers: { "Content-Type": "application/json" },
      data: { url: "http://alipi.us/site_guide.html#target" },
    };

    axios
      .request(options)
      .then(function (response) {
        console.log(response);
        console.log(response.data);
        // console.log(response.data.json());
        setArticle(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  return (
    <div>
      <form
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <label>
          URL:
          <input
            type="text"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
          />
        </label>
        <button type="submit">Parse Article</button>
      </form>
      {article && (
        <div>
          <h1>{article?.title}</h1>
          {/* <p>{article?.content}</p> */}
          <div dangerouslySetInnerHTML={{ __html: article?.content }} />
        </div>
      )}
    </div>
  );
};
export default Ren;
