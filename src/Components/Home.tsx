import React, { useEffect, useState } from "react";
import { browser } from "webextension-polyfill-ts";
import axios from "axios";
import Ren from "./Ren";
import "tailwindcss/tailwind.css";

const getActiveTab = async () => {
  const [tab] = await browser.tabs.query({ active: true, currentWindow: true });
  return tab.url;
};

const Home = () => {
  const baseURL = localStorage.getItem("prod_url");
  const uiURL = localStorage.getItem("ui_url");
  const [data, setData] = useState<any>("");
  const [ren, setRen] = useState<any>("");
  const [responseCount, setResponseCount] = useState(0);

  useEffect(() => {
    const handleClick = async () => {
      const url = await getActiveTab();
      setData(url);
    };

    handleClick();
  }, []);

  useEffect(() => {
    if (data !== "" && data !== undefined) {
      checkForRenarration(data);
    }
  }, [data]);

  const checkForRenarration = (url: string) => {
    const options = {
      method: "GET",
      url: `${baseURL}/renarration?url=${url}`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        setResponseCount(response.data.length);
        // const browserAPI =
        //   typeof browser !== "undefined" ? browser : chrome;
        // browserAPI &&
        //   browserAPI.browserAction.setBadgeText({
        //     text: response.data.length.toString(),
        //   });
        setRen(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const renderNode = (ren: any, type: any) => {
    const node = document.evaluate(
      ren.target.value,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    ).singleNodeValue;
    if (node && type === "kn") {
      if (ren.motivation === "kan-text") {
        node.textContent = ren.body.value;
      } else if (ren.motivation === "kan-img") {
        (node as HTMLImageElement).src = ren.body.value;
      }
    } else if (node && type === "hi") {
      if (ren.motivation === "hin-text") {
        node.textContent = ren.body.value;
      } else if (ren.motivation === "hin-img") {
        (node as HTMLImageElement).src = ren.body.value;
      }
    }
  };

  const handleButtonClick = () => {
    browser.tabs
      .query({ active: true, currentWindow: true })
      .then((tabs) => {
        const activeTab = tabs[0];
        const url = activeTab?.url;

        if (url) {
          browser.tabs.create({ url });
        }
      })
      .catch((error) => console.error(error));
  };

  const ActiveTab = () => {
    const [content, setContent] = useState<any>("");

    const handleButtonClick = async () => {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then((tabs) => {
          const activeTab = tabs[0];
          const url = activeTab?.url;
          const tabId = activeTab?.id;

          if (url) {
            setContent(url);

            // Update the iframe to display the contents of the active tab
            // const iframe = document.getElementById(
            //   "content-iframe"
            // ) as HTMLIFrameElement;
            // iframe.src = url;

            // browser.tabs.create({ url });

            if (tabId) {
              axios.get(url).then((response) => {
                const { data: html } = response;

                axios.get(url).then((response) => {
                  const { data: css } = response;

                  axios.get(url).then((response) => {
                    const { data: js } = response;

                    browser.tabs.executeScript(tabId, {
                      code: `
                          const style = document.createElement('style');
                          style.textContent = \`${css}\`;
                          document.head.append(style);
      
                          const container = document.createElement('div');
                          container.innerHTML = \`${html}\`;
                          document.body.append(container);
      
                          const script = document.createElement('script');
                          script.textContent = \`${js}\`;
                          document.body.append(script);
                        `,
                    });
                  });
                });
              });
            }
          }
        })
        .catch((error) => console.error(error));
    };

    const renderActiveTab = async () => {
      browser.tabs
        .query({ active: true, currentWindow: true })
        .then(async (tabs) => {
          const activeTab = tabs[0];
          const url = activeTab?.url;
          const tabId = activeTab?.id;

          browser.tabs.executeScript(activeTab.id, {
            code: `console.log('Hello, world!')`,
          });
          const result = await browser.tabs.executeScript(activeTab.id, {
            code: "document.documentElement.outerHTML",
          });

          setContent(result[0]);
        });
    };

    return (
      <div>
        {/* <button onClick={handleClick}>Get URL</button> */}
        <div className="container mx-auto p-4 bg-gray-200 rounded-md">
          <p className="text-lg font-bold">SWeEts Web Browser plugin</p>
          <p className="px-4 py-2 mt-4 bg-blue-500 text-white rounded-md">
            {data}
          </p>
        </div>
        {/* {data !== "" && <Ren data={data} />} */}
      </div>
    );
  };

  return (
    <div className="flex flex-col w-full text-gray-700 container ">
      <ActiveTab />
      {ren.length !== 0 &&
        ren.length > 0 &&
        ren.map((ren: any, key: any) => (
          <div
            key={key}
            onClick={() => {
              console.log("Clicked " + key);
              console.log(`${uiURL}/renarrate/${ren?.uuid}`);
              window.open(`${uiURL}/renarrate/${ren?.uuid}`, "_blank");
            }}
          >
            {ren?.title}
          </div>
        ))}
    </div>
  );
};

export default Home;
