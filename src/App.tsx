import axios from "axios";
import React, { useEffect } from "react";
import Home from "./Components/Home";

const App = () => {
  useEffect(() => {
    const baseURL = () => {
      const options = {
        method: "GET",
        url: process.env.PUBLIC_URL + `/config/config.json`,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      axios
        .request(options)
        .then((response) => {
          localStorage.setItem("prod_url", response.data.API_URL);
          localStorage.setItem("read_url", response.data.READABILITY_URL);
          localStorage.setItem("ui_url", response.data.UI_URL);
        })
        .catch((error) => {
          console.error(error);
        });
    };

    baseURL();
  }, []);

  return (
    <div className="App">
      <Home />
    </div>
  );
};

export default App;
